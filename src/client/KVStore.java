package client;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import logger.LogSetup;
import common.messages.KVMessage;
import common.messages.MetaMessage;
import common.messages.MetadataEntry;
import common.messages.KVMessage.StatusType;
import common.messages.TextMessage;
import common.messages.TextMessage.MessageParameters;


public class KVStore implements KVCommInterface {

	private Logger logger = Logger.getRootLogger();
	
	// Store the consistent hashing metadata entries:
	List<MetadataEntry> metadata_entries = null;
	
	
	// Connection parameters
	private String serverAddress;
	private int serverPort;
	private boolean running;
	public enum SocketStatus{CONNECTED, DISCONNECTED, CONNECTION_LOST};
	private Socket clientSocket;
	private OutputStream output;
 	private InputStream input;
	

 	// Receiver buffer/message parameters:
	private static final int BUFFER_SIZE = 1024;
	private static final int DROP_SIZE = 1024 * BUFFER_SIZE;
	public static final byte HEADER_OPEN = 0x7E;
	public static final byte HEADER_CLOSE = 0x5E;
	public static final byte MESSAGE_STUFFING = 0x23;

	
	
 	
 	
 	
	/**
	 * Initialize KVStore with address and port of KVServer
	 * 
	 * @param address the address of the KVServer
	 * @param port the port of the KVServer
	 */
	public KVStore(String address, int port) {
		this.serverAddress = address;
		this.serverPort = port;
	}
	
	/**
	 * Connects client to the server in order to send and receive messages
	 */
	public void connect() throws Exception {
		clientSocket = new Socket(this.serverAddress, this.serverPort);
		
		try {
			output = clientSocket.getOutputStream();
			input = clientSocket.getInputStream();
			setRunning(true);
			TextMessage connect_response = receiveMessage();
			
			if(connect_response != null){
				// Client did not fail to read response, ie server disconnect
				System.out.println(connect_response.getMsg()); // Server informs client they have established a connection
			}
			else{
				System.out.println("Server disconnected during connection phase.");
				setRunning(false);
			}
		} 
		
		catch (IOException ioe) {
			System.out.println("Input and output streams to server could not be established!");
			logger.error("Input and output stream could not be established!");
			System.exit(1);
		} 
	}


	/**
	 *  Disconnect client communication from the server and cleans up resources used.
	 */
	public void disconnect() {
		
		try{
			setRunning(false);
			logger.info("tearing down the connection ...");
			if (clientSocket != null) {
				input.close();
				output.close();
				clientSocket.close();
				clientSocket = null;
				logger.info("connection closed!");
			}
		}
		catch (IOException ioe) {
			logger.error("Unable to close connection!");
		}
	}
	
	
	
	/**
	 * 
	 * just used to call put_func with a loop for server redirections
	 * 
  	 * @param key: key associated with tuple to insert in server table
	 * @param value: value associated with tuple to insert in server table
	 */
	public KVMessage put(String key, String value) throws Exception{
		
		KVMessage response = null;
		
		// Send constructed put value:
		int counter = 0;
		do{
			response = this.put_func(key, value);
			if(response == null)
				System.out.println("response is null");

			if(counter==1)
				System.out.println("\nretry..." + counter + " times");
			else if(counter>1)//client should only re-try 1 time. if all the servers have the updated metadata.
				break;
			counter++;
		}while(response.getStatus()==StatusType.SERVER_NOT_RESPONSIBLE || response.getStatus()==StatusType.SERVER_WRITE_LOCK || response.getStatus()==StatusType.SERVER_STOPPED);
		
		return response;
		
	}
	
	
	

	/**
	 * 
	 * Attempts to insert a <key,value> tuple into the server database.
	 * 
  	 * @param key: key associated with tuple to insert in server table
	 * @param value: value associated with tuple to insert in server table
	 */
	public KVMessage put_func(String key, String value) throws Exception {

		TextMessage request = new TextMessage(null, key, value ,StatusType.PUT, false, false);	
		
		sendMessage(request);
		TextMessage response = receiveMessage();
		
		
		// Ie. server disconnect:
		if(response == null){
			setRunning(false);
		}
		else{
			
			//System.out.println("this is called" + response.getMsg());
			
			if(response.getStatus() == StatusType.SERVER_STOPPED){//assume the stopped servers still have the most updated metadata
				
				System.out.println("The storage server is stopped.");
				
				//then update the metadata entries
				String metadata_msg = response.getMsg();
				Gson gson = new Gson();
				
				//Type listType = new TypeToken<List<MetadataEntry>>(){}.getType();
				MetaMessage msg = gson.fromJson(metadata_msg, MetaMessage.class);
				metadata_entries = msg.metadata_entries;
				
				//reconnect to the right server
				disconnect();
				//System.out.println("disconnected");
				reconnect_to_correct_server(key);
				
				System.out.println("reconnected to server: server addr: " + this.serverAddress + ", server port number: " + this.serverPort);
				
				
				// setRunning(false);
				
				
			}else if(response.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE){
				
				System.out.println("Server is not responsible. Reconnecting to the responsible server...");
				
				//then update the metadata entries
				String metadata_msg = response.getMsg();
				Gson gson = new Gson();
				
				//Type listType = new TypeToken<List<MetadataEntry>>(){}.getType();
				MetaMessage msg = gson.fromJson(metadata_msg, MetaMessage.class);
				metadata_entries = msg.metadata_entries;
				
				//reconnect to the right server
				disconnect();
				//System.out.println("disconnected");
				reconnect_to_correct_server(key);
				
				System.out.println("reconnected to server: server addr: " + this.serverAddress + ", server port number: " + this.serverPort);
				
			}else if(response.getStatus() == StatusType.SERVER_WRITE_LOCK){
				
				//System.out.println("Server is currently blocked for write requests due to reallocation of internal data.");
				
				//then update the metadata entries
				String metadata_msg = response.getMsg();
				Gson gson = new Gson();
				
				//Type listType = new TypeToken<List<MetadataEntry>>(){}.getType();
				MetaMessage msg = gson.fromJson(metadata_msg, MetaMessage.class);
				metadata_entries = msg.metadata_entries;
				
				System.out.println("reconnected to server: server addr: " + this.serverAddress + ", server port number: " + this.serverPort);
				
				Thread.sleep(1000);//wait for one second then try again
				
				
//				System.out.println("-------------------------------------------");
//				System.out.println(metadata_entries.get(0));
//				System.out.println("-------------------------------------------");
			}else{
				
				//successful
				
			}
			
		}
		return response;
	}
	
	
	/**
	 * 
	 * reconnect to the correct server 
	 * @param key: is used to find which server I should connect to
	 * 
	 */
	public void reconnect_to_correct_server(String key){
		
//		System.out.println("this is called");
//		if(metadata_entries == null){
//			System.out.println("this is not working");
//		}
//		if(metadata_entries.isEmpty()){
//			System.out.println("metadata entries are empty");
//		}else{
//			System.out.println("metadata entries are NOT empty");
//		}
//		
//		try{
//			MetadataEntry meta = metadata_entries.get(0);
//		}catch(Exception e){
//			System.out.println(e.getMessage());
//		}
//		System.out.println("hello");
		
		for(MetadataEntry metadata: metadata_entries){
			
			//System.out.println("trying..........");
			
			if(is_key_in_range(key, metadata)){//this is the metadata of the server that we need to connect to 
				
				this.serverAddress = metadata.hostname;
				this.serverPort = metadata.port;
				
				//System.out.println("trying server addr " + this.serverAddress + " server port " + this.serverPort);
				
				try{
					connect();
				} catch (IOException e) {
					System.out.println("Could not establish connection!");
					//logger.warn("Could not establish connection!", e);
				}catch(Exception e){
					System.out.println("Unknown exception occured.");	
				}
				
				
				break;
			}
			
		}
		
		
		
	}
	
	
	/**
	 * 
	 * helper function to determine if the key is within the server's range
	 * 
	 */
	public boolean is_key_in_range(String key, MetadataEntry metadata){
		
    	// Used to create hashes:
		MessageDigest md = null;
		try{
			md = MessageDigest.getInstance("MD5");
		}
		catch(NoSuchAlgorithmException e){
			System.out.println("The MD5 algorithm does not exist...");
			System.exit(1);
		}
		
		// Generate hash:
		byte[] to_hash = key.getBytes();
		md.update(to_hash);
		byte[] key_hash = md.digest();
		
		int md_length = md.getDigestLength();
		if(md_length<0){
			logger.error("something went wrong - md hash length is negative");
		}
		
		byte[] zeros = new byte[md_length];
		// //Arrays.fill(ring_size_minus_one, (byte)255);
		for(int i=0; i<md_length; i++){

			zeros[i] = (byte)0;

		}

		byte[] ring_size = new byte[md_length];
		// //Arrays.fill(ring_size_minus_one, (byte)255);
		for(int i=0; i<md_length; i++){

			ring_size[i] = (byte)255;

		}
		
		//check the undetectable region first
		if(compare(key_hash, zeros)>0  && compare(key_hash, metadata.hash)<=0){
			if(metadata == metadata_entries.get(0))
				return true;
		}

		if(compare(key_hash, ring_size)<=0 && compare(key_hash, metadata.prev_hash_range)>0){
			if(metadata == metadata_entries.get(0))
				return true;
		}


		//if key is not within the undetectable range and it's the normal case
		if(compare(key_hash,metadata.prev_hash_range)>0 && compare(key_hash,metadata.hash)<=0){
			//logger.info("key is in range");
			return true;
		}else{
			//logger.info("key is not in range");
			return false;
		}
		
	
	}
	
	
    /**
     * 
     * Lexicographically compares two byte arrays
     * 
     * @param left_hash: Left hash to compare to right hash
     * @param right_hash
     * 
     * @return: int > 0  : Left comes after right
     * 			int == 0 : They are equal
     * 			int < 0  : Left comes before right
     */
    public int compare(byte[] left_hash, byte[] right_hash) {
		
		/*
		 * Determines which hash comes first
		 */
		for (int i = 0, j = 0; i < left_hash.length && j < right_hash.length; i++, j++) {
			int a = (left_hash[i] & 0xff);
			int b = (right_hash[j] & 0xff);
			if (a != b) {
				return a - b;
			}
		}
		return left_hash.length - right_hash.length;
	}
	
	
	
	/**
	 * 
	 * stub used to call get handling server redirections
	 * 
  	 * @param key: key associated with tuple to retrieve from server
	 */
	public KVMessage get(String key) throws Exception {
	
		KVMessage response = null;
		// Send constructed put value:
		int counter = 0;
		do{
			response = this.get_func(key);
			if(counter==1)
				System.out.println("\nretry..." + counter + " times");
			else if (counter >1)//client should only re-try 1 time. if all the servers have the updated metadata.
				break;
			counter++;
		}while(response.getStatus()==StatusType.SERVER_NOT_RESPONSIBLE || response.getStatus()==StatusType.SERVER_STOPPED);
		
		return response;
	
	}
	

	/**
	 * 
	 * Attempts to retrieve a <key,value> tuple from the server database using a key.
	 * 
  	 * @param key: key associated with tuple to retrieve from server
	 */
	public KVMessage get_func(String key) throws Exception {
		
		TextMessage request = new TextMessage(null, key, null ,StatusType.GET, false, false);
		sendMessage(request);
		TextMessage response = receiveMessage();
		
		// Ie. server disconnect:
		if(response == null){
			setRunning(false);
		}
		else{
			
			if(response.getStatus() == StatusType.SERVER_STOPPED){

				System.out.println("The storage server is stopped.");
				
				//then update the metadata entries
				String metadata_msg = response.getMsg();
				Gson gson = new Gson();
				
				//Type listType = new TypeToken<List<MetadataEntry>>(){}.getType();
				MetaMessage msg = gson.fromJson(metadata_msg, MetaMessage.class);
				metadata_entries = msg.metadata_entries;
				
				//reconnect to the right server
				disconnect();
				//System.out.println("disconnected");
				reconnect_to_correct_server(key);
				
				System.out.println("reconnected to server: server addr: " + this.serverAddress + ", server port number: " + this.serverPort);
				

			}else if(response.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE){
				
				System.out.println("Server is not responsible. Reconnecting to the responsible server...");
				
				//then update the metadata entries
				String metadata_msg = response.getMsg();
				Gson gson = new Gson();
				
				//Type listType = new TypeToken<List<MetadataEntry>>(){}.getType();
				MetaMessage msg = gson.fromJson(metadata_msg, MetaMessage.class);
				metadata_entries = msg.metadata_entries;
				
				//reconnect to the right server
				disconnect();
				//System.out.println("disconnected");
				reconnect_to_correct_server(key);
				
				System.out.println("reconnected to server: server addr: " + this.serverAddress + ", server port number: " + this.serverPort);
				
			}
			
		}
		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	/*
	 * 
	 * 
	 * HELPER FUNCTIONS
	 *
	 */
	
	
	/**
	 * Close the connection (Change state)
	 */
	public synchronized void closeConnection() {
		logger.info("try to close connection ...");
		
		try {
			tearDownConnection();
			handle_status(SocketStatus.DISCONNECTED);
		} catch (IOException ioe) {
			logger.error("Unable to close connection!");
		}
	}
	
	/**
	 * Closes the client connection
	 * 
	 * @throws IOException: We attempt to close a socket that can't be closed.
	 */
	private void tearDownConnection() throws IOException {
		setRunning(false);
		logger.info("tearing down the connection ...");
		if (clientSocket != null) {
			clientSocket.close();
			clientSocket = null;
			logger.info("connection closed!");
		}
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public void setRunning(boolean run) {
		running = run;
	}
	
	
	/**
	 * Method sends a TextMessage using this socket.
	 * @param msg the message that is to be sent.
	 * @throws IOException some I/O error regarding the output stream 
	 */
	public void sendMessage(TextMessage msg) throws IOException {
		byte[] msgBytes = msg.getMsgBytes(); 
		String string_format = new String(msgBytes);
		output.write(msgBytes, 0, msgBytes.length);
		output.flush();
		logger.info("Send message:\t '" + string_format + "'");
    }
	
	
	
	
	
	
	
	
	
	/**
	 * Attempts to wait and read a request from a client.
	 * 
	 * @return: TextMessage - formatted message received 
	 * 			NULL - indicating a closed connection
	 * 
	 * @throws IOException: If there is an external failure reading from the socket
	 */
	private TextMessage receiveMessage() throws IOException {
		

		// Message to return is initially null to indicate failure reading bytes:
		TextMessage msg = null;
		
		
		int index = 0;
		byte[] msgBytes = null, tmp = null;
		byte[] bufferBytes = new byte[BUFFER_SIZE];
		byte cur_byte = 0x0;
		byte prev = 0x0;
		
		/* read first char from stream */
		boolean reading = true;


		// While we haven't read the opening header for a message, keep reading before we record anything
		//
		while(!(cur_byte == HEADER_OPEN && prev != MESSAGE_STUFFING) && reading){
			prev = cur_byte;
			cur_byte = (byte)input.read();
			

			if(cur_byte == -1){
				reading = false;
				running = false;
			}
		}
		
		// Error while reading, ie server close:
		if(!running)
			return msg;
		
		
		
		/* read next char from stream */
		cur_byte = (byte) input.read();
		
				
				
		// While we haven't read the closing header for the message, record everything, this is the JSON object
		//
		boolean stuff_last_char = false;
		
		while(!(cur_byte == HEADER_CLOSE &&  prev!= MESSAGE_STUFFING) && reading) {
			
			
			// IF we enter and we previously skipped writing because of a stuff
			// The stuff is an actual character within the message:
			//
			if(stuff_last_char){

				// This was not an attempt to stuff a header open either:
				//
				if(cur_byte != HEADER_OPEN){
					bufferBytes[index] = cur_byte;
					prev = cur_byte;
							
					index++;
				}
			}
			
			
			
			// Update whether the current character is a stuff character
			if(cur_byte == MESSAGE_STUFFING)
				stuff_last_char = true;
			else
				stuff_last_char = false;

				
			
			
			if(!stuff_last_char){
					
				/* CR, LF, error */
				/* if buffer filled, copy to msg array */
				if(index == BUFFER_SIZE) {
					if(msgBytes == null){
						tmp = new byte[BUFFER_SIZE];
						System.arraycopy(bufferBytes, 0, tmp, 0, BUFFER_SIZE);
					} else {
						tmp = new byte[msgBytes.length + BUFFER_SIZE];
						System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
						System.arraycopy(bufferBytes, 0, tmp, msgBytes.length,
								BUFFER_SIZE);
					}
	
					msgBytes = tmp;
					bufferBytes = new byte[BUFFER_SIZE];
					index = 0;
				} 
				
				/* only read valid characters, i.e. letters and constants */
				bufferBytes[index] = cur_byte;						
				index++;
				
				/* stop reading is DROP_SIZE is reached */
				if(msgBytes != null && msgBytes.length + index >= DROP_SIZE) {
					reading = false;
				}
			
			}
			
			prev = cur_byte;
			/* read next char from stream */
			cur_byte = (byte) input.read();
			
			if(cur_byte == -1){
				reading = false;
				running = false;
			}
				
		}
		
		// Error reading bytes, ie server close:
		if(!running)
			return msg;
		
		
		
		if(msgBytes == null){
			tmp = new byte[index];
			System.arraycopy(bufferBytes, 0, tmp, 0, index);
		} else {
			tmp = new byte[msgBytes.length + index];
			System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
			System.arraycopy(bufferBytes, 0, tmp, msgBytes.length, index);
		}
		
		
		// MsgBytes contains actual message which is a JSON string
		//
		// We convert the JSON string into a MessageParameters object for convenience
		//
		msgBytes = tmp;
		String string_format = new String(msgBytes);
		
		Gson gson = new Gson();
		MessageParameters json_parameters = gson.fromJson(string_format, MessageParameters.class);
		
		
		/* build final String */
		msg = new TextMessage(json_parameters.get_msg(), json_parameters.get_key(), json_parameters.get_value(), json_parameters.get_status(), false, false);
		
		logger.info("RECEIVE \t<" 
				+ clientSocket.getInetAddress().getHostAddress() + ":" 
				+ clientSocket.getPort() + ">: '" 
				+ string_format + "'");
				
		return msg;		

    }
	
	
	public void handle_status(SocketStatus status) {
		if(status == SocketStatus.CONNECTED) {

		} else if (status == SocketStatus.DISCONNECTED) {
			//System.out.print(PROMPT);
			System.out.println("Connection terminated: " 
					+ serverAddress + " / " + serverPort);
			
		} else if (status == SocketStatus.CONNECTION_LOST) {
			System.out.println("Connection lost: " 
					+ serverAddress + " / " + serverPort);
			//System.out.print(PROMPT);
		}
		
	}
	 
}