package app_kvServer;


import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.File;
import java.io.IOException;

import logger.LogSetup;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import app_kvEcs.ECSFunc.ServerConnection;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.PriorityQueue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.net.SocketException;

import java.util.List;
import java.util.ArrayList;
import common.messages.MetadataEntry;
import javax.xml.bind.DatatypeConverter;
import java.math.BigInteger;
import java.util.Arrays;



public class KVServer extends Thread{
	
	// In memory cache shared among client threads spawned:
	private volatile Cache data_cache;

	Logger logger = Logger.getRootLogger();

	// Parameters input into the command line:
	private int port;
    private int cachesize;
    private String strategy;  
    
    
    // Persistent storage parameters:
    public static String storage_filename = "./server_storage.txt"; 
    private static ReadWriteLock storage_lock;
    
    
    
    
    // Server socket to accept connections:
	private ServerSocket serverSocket;
	private boolean running;
    
	
	
	
	// Store the consistent hashing metadata entries:
	List<MetadataEntry> metadata_entries = null;
	Boolean service_started = false;
	String server_hostname = null; // Hostname/IP of this server
	
	
	
	// The server is responsible for sending heartbeat messages to the \
	// next server in the ring
	// (For failure detection)
	//
	MetadataEntry server_buddy = null;
	FailureDetector heartbeat_thread = null;
	String failure_server_hostname = null; // Used to alert of failures to ECS
	int failure_server_port = -1;
	
	
	
	
	public Boolean is_write_locked;
	
	/**
	 * Start KV Server at given port
	 * @param port given port for storage server to operate
	 * @param cacheSize specifies how many key-value pairs the server is allowed 
	 *           to keep in-memory
	 * @param strategy specifies the cache replacement strategy in case the cache 
	 *           is full and there is a GET- or PUT-request on a key that is 
	 *           currently not contained in the cache. Options are "FIFO", "LRU", 
	 *           and "LFU".
	 */
	public KVServer(int port, int cacheSize, String strategy) {
		this.port = port;
		this.cachesize = cacheSize;
		this.strategy = strategy;
	}
	
	/**
     * Initializes and starts the server. 
     * Loops until the the server should be closed.
     */
    public void run() {
    	
    	
    	// Check for persistent storage:
    	//
		File f = new File(storage_filename);
		if(!f.exists() || f.isDirectory()) {//create file in current directory
		    
			logger.info("New Storage file Created\n");
			
			Path file = Paths.get(storage_filename);
			try {
			    Files.createFile(file);
			} catch (FileAlreadyExistsException x) {
			    System.err.format("file named %s" +
			        " already exists%n", file);
			} catch (IOException x) {
			    System.err.format("createFile error: %s%n", x);
			    logger.warn("createFile error: %s\n", x);
			}
			
			
		}else{
			logger.info("Using existing storage file" + f.getName());
		}

		//create persistent storage file lock
		//storage_lock = new ReadWriteLock[1];
		storage_lock = new ReentrantReadWriteLock();
    	
        
		//Create data cache
		data_cache = new Cache(cachesize, strategy);
		
		//initialize write lock
		is_write_locked = false;
		
		
		
		
		
		
    	running = initializeServer();
        if(serverSocket != null) {
	        while(isRunning()){
	            try {
	            	
	            	
	            	// Its possible we're running but the socket is null:
	            	if(serverSocket != null){
	            		
	            		
	            		
	            		Socket client = null;
	            		try{
	            			client = serverSocket.accept();      
	            		}
	            		catch(SocketException e){
	            			logger.info("Closed the server in accept call. \n", e);
	            			// Do nothing, we only get here is the socket closes during the accept() call
	            			// Only happens during performance testing calling serverClose()
	            		}
	            		
	            		
		                if(serverSocket != null && client != null){
			                ClientConnection connection = new ClientConnection(client, data_cache, storage_lock, this);
			                Thread new_thread = new Thread(connection);
			                new_thread.setDaemon(true);
			                new_thread.start();


			                logger.info("Connected to " 
			                		+ client.getInetAddress().getHostName() 
			                		+  " on port " + client.getPort());
		                }
	            	}
	                
	            } catch (IOException e) {
	            	logger.error("Error! " +
	            			"Unable to establish connection. \n", e);
	            }
	        }
        }
        logger.info("Server stopped.");
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     * 
     * 
     * 
     * 
     * 
     *  Helper functions
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    
    private boolean isRunning() {
        return this.running;
    }

    /**
     * Stops the server insofar that it won't listen at the given port any more.
     * (Note: Called by a thread that receives shutdown request from ECS)
     */
    public void stopServer(){
        try {
			serverSocket.close();
			serverSocket = null;
			logger.info("Server terminated\n");
		} catch (IOException e) {
			logger.error("Error! " +
					"Unable to close socket on port: " + port, e);
		}
        running = false;
    }

    /**
     *  Updates the server's consistent hashing meta data:
     *  (Note: Called by a thread that receives initialize request from the ECS)
     *  (Note2: Also sets server_buddy for failure detection) 
     *  
     *  @param new_metadata: The initial metadata received from the server.
     */
    public void initialize_server_metadata(List<MetadataEntry> new_metadata, String _failure_hostname, int _failure_port){

    	// Update metadata:
		this.metadata_entries = new_metadata;
		
		
		// Set failure server contact:
		this.failure_server_hostname = _failure_hostname;
		this.failure_server_port = _failure_port;
		
    	// Nobody to look after:
		if(metadata_entries.size() == 1){
			server_buddy = null;
			return;
		}
			
	
		
		// Look for our metadata in list of metadata ---> get the server right after:
		// (For failure detection)
		// 
		MetadataEntry our_metadata = null;
		MetadataEntry old_server_buddy = server_buddy;
		server_buddy = null;
		
		for(MetadataEntry cur_metadata : metadata_entries){
		 
			if(our_metadata != null && server_buddy == null)
		 		server_buddy = cur_metadata;
			
		 	// Check if it is the right entry:
		 	if( server_hostname.equals(cur_metadata.hostname) &&  port==cur_metadata.port)
		 		our_metadata = cur_metadata;
		
		}
		
		
		// Check if buddy is set:
		//
		if(our_metadata == null){
			logger.error("Could not find our metadata in the metadata list.");
			return;
		}
		else{
			 
			// Ie. we are the last server, buddy server is the first in the list:
			if(server_buddy == null){
		 		 server_buddy = metadata_entries.get(0);
		 	 }
		}
		 
		 
		 
		 
		// If we have a new server buddy (ie, a new server to track, start a failure detector thread):
		//
		if(server_buddy != null && ((old_server_buddy == null) || 
		 		 (!(old_server_buddy.hostname.equals(server_buddy.hostname) && old_server_buddy.port==server_buddy.port)) ))
		 
		{
		 	 if(heartbeat_thread != null)
		 		 heartbeat_thread.stop_tracking(); // Close the old server tracker
			 
			 
		 	 
			 logger.info("\n\nStarting a new failure detector to track ("+ server_buddy.hostname + "," +  server_buddy.port + ")\n\n");
		 	 // Create a new failure detector to track a server:
		 	 //
		 	 heartbeat_thread = new FailureDetector(server_buddy.hostname, server_buddy.port, failure_server_hostname, failure_server_port);
		 	 heartbeat_thread.setDaemon(true);
		 	 heartbeat_thread.start();
		}
		
    }
    
    /**
     *  Allows a server to start respond to request from clients (put/gets).
     */
    public void start_service(){
    	this.service_started = true;
    }
    
    /**
     *  Stops a server from responding to requests from clients (put/gets).
     */
    public void stop_service(){
    	this.service_started = false;
    	
    	// Stop caring about failure detection:
    	if(heartbeat_thread != null)
    		heartbeat_thread.stop_tracking();
    	heartbeat_thread = null;
    }
    
    /**
     * @return: The status of the storage service.
     */
    public Boolean get_service_status(){
    	return this.service_started;
    }
    
    /**
     * Accessor functions for the server's hostname:
     * 
     * @param _hostname: The hostname of this server
     */
    public void set_hostname(String _hostname){
    	this.server_hostname = _hostname;
    }
    public String get_hostname(){
    	return this.server_hostname;
    }
    
    
    /**
     * 
     * Determines if the server should move this key
     * 
     * @param key: Key we wish to check
     * @return: True/false - the key is in in the server's range
     */
    public Boolean is_in_move_range(String key, byte[] start_range, byte[] end_range){
    	
    	// Used to create hashes:
		MessageDigest md = null;
		try{
			md = MessageDigest.getInstance("MD5");
		}
		catch(NoSuchAlgorithmException e){
			System.out.println("The MD5 algorithm does not exist...");
			System.exit(1);
		}

		 
		// Generate hash:
		 byte[] to_hash = key.getBytes();
	     md.update(to_hash);
	     byte[] key_hash = md.digest();
	     
	     
	     

		// Check ranges:
		int md_length = md.getDigestLength();
		if(md_length<0){
			logger.error("something went wrong - md hash length is negative");
		}
		
		// Array of 0's and and array of F's to use later:
		byte[] zeros = new byte[md_length];
		byte[] ring_size = new byte[md_length];
		for(int i=0; i<md_length; i++){
			zeros[i] = (byte)0;
			ring_size[i] = (byte)255;
		}



		// Check corner case;
		// (Start comes after end range because of the circle discontinuity)
		//
		if((compare(start_range, end_range) > 0 ) && ( (compare(key_hash, start_range)>=0) || (compare(key_hash, end_range) <=0) ) ) 
			return true;
			
		// "Normal case"
		// Start before end
		//
		if((compare(start_range, end_range) < 0 ) && ( (compare(key_hash, start_range)>=0) && (compare(key_hash, end_range) <=0) ) ) 
			return true;
		
		
		return false;
	}
    
    
    
    
    /**
     * 
     * Determines if the server is responsible for this key
     * 
     * @param key: Key we wish to check
     * @return: True/false - the key is in in the server's range
     */
    public boolean is_key_in_range(String key){
    	
    	// Used to create hashes:
		MessageDigest md = null;
		try{
			md = MessageDigest.getInstance("MD5");
		}
		catch(NoSuchAlgorithmException e){
			System.out.println("The MD5 algorithm does not exist...");
			System.exit(1);
		}

		 
		// Generate hash:
		 byte[] to_hash = key.getBytes();
	     md.update(to_hash);
	     byte[] key_hash = md.digest();
	     
	     
	     
	     // Look for our metadata in list of metadata:
	     MetadataEntry our_metadata = null;
	     for(MetadataEntry cur_metadata : metadata_entries){
	    	 
	    	 // Check if it is the right entry:
	    	 if( server_hostname.equals(cur_metadata.hostname) &&  port==cur_metadata.port)
	    		 our_metadata = cur_metadata;

	     }
	     if(our_metadata == null){
	    	 logger.error("Could not find our metadata in the metadata list.");
	    	 return false;
	     }
	     

		// Check ranges:
		int md_length = md.getDigestLength();
		if(md_length<0){
			logger.error("something went wrong - md hash length is negative");
		}
		
		// Array of 0's and and array of F's to use later:
		byte[] zeros = new byte[md_length];
		byte[] ring_size = new byte[md_length];
		for(int i=0; i<md_length; i++){
			zeros[i] = (byte)0;
			ring_size[i] = (byte)255;
		}

		print_hash_info(key_hash, our_metadata.prev_hash_range, our_metadata.hash);


		//check the undetectable region first
		if(compare(key_hash, zeros)>0  && compare(key_hash, our_metadata.hash)<=0){
			if(our_metadata == metadata_entries.get(0))
				return true;
		}

		if(compare(key_hash, ring_size)<=0 && compare(key_hash, our_metadata.prev_hash_range)>0){
			if(our_metadata == metadata_entries.get(0))
				return true;
		}


		//if key is not within the undetectable range and it's the normal case
		if(compare(key_hash,our_metadata.prev_hash_range)>0 && compare(key_hash,our_metadata.hash)<=0){
			logger.info("key is in range");
			return true;
		}else{
			logger.info("key is not in range");
			return false;
		}




    }


    /**
     * 
     * Determines if the server is coordinator for this key
     * 
     * @param key: Key we wish to check
     * @return: True/false - if the server is the coordinator
     */
    public boolean is_current_server_coordinator(String key){

    	return is_key_in_range(key);

    }



    /**
     * 
     * Determines if the server is responsible for the key because it's a replica
     * The server is a replica if and only if it is a successor or second successor of the coordinator server
     * 
     * @param key: Key we wish to check
     * @return: True/false - if the server is the replica server
     */
    public boolean is_current_server_replica(String key){

		logger.info("--------------------find out if the current server is a replica--------------------------------");
	
    	//first find the coordinator server
	     int coord_index = -1;
	     for(int i=0; i < metadata_entries.size(); i++){
	    	 
	    	 if(is_given_server_coordinator(key, metadata_entries.get(i))){
				 coord_index = i;
				 break;
			 }

	     }
		
		if(coord_index==-1){
			logger.error("there is no coordinator for the given key");
		}else{
			logger.info("the coordinator server found is " + coord_index + " for key " + key);
			logger.info("coordinator is at port " + metadata_entries.get(coord_index).port);
		}
		
		// find our server metadata
	    // Look for our metadata in list of metadata:
		MetadataEntry our_metadata = null;
		int our_server_index = 0;
		for(MetadataEntry cur_metadata : metadata_entries){

			// Check if it is the right entry:
			if( server_hostname.equals(cur_metadata.hostname) &&  port==cur_metadata.port){
				our_metadata = cur_metadata;
				break;
			}
			our_server_index++;

		}
		if(our_metadata == null){
			logger.error("Could not find our metadata in the metadata list.");
			return false;
		}
		
		logger.info("our server is at index " + our_server_index);
		logger.info("our server is at port " + metadata_entries.get(our_server_index).port);
		
		//then check if our server is the successor or the second successor of the coordinator server
		if(is_first_successor(our_server_index, coord_index)){
			logger.info("our server is the first successor");
			return true;
		}else if(is_second_successor(our_server_index, coord_index)){
			logger.info("our server is the second successor");
			return true;
		}else{
			logger.info("our server is not a successor");
			return false;
		}
		
    }

	
	/**
	* 
	* find the coordinator server
	* 
	* 
	* @param key: Key we wish to check
	* @return: the coordinator server's position in medatadata_entries
	*/
    public boolean is_given_server_coordinator(String key, MetadataEntry metadata){
    
    	// Used to create hashes:
		MessageDigest md = null;
		try{
			md = MessageDigest.getInstance("MD5");
		}
		catch(NoSuchAlgorithmException e){
			System.out.println("The MD5 algorithm does not exist...");
			System.exit(1);
		}

		// Generate hash:
		byte[] to_hash = key.getBytes();
		md.update(to_hash);
		byte[] key_hash = md.digest();
		 

		// Check ranges:
		int md_length = md.getDigestLength();
		if(md_length<0){
			logger.error("something went wrong - md hash length is negative");
		}
		
		// Array of 0's and and array of F's to use later:
		byte[] zeros = new byte[md_length];
		byte[] ring_size = new byte[md_length];
		for(int i=0; i<md_length; i++){
			zeros[i] = (byte)0;
			ring_size[i] = (byte)255;
		}

		print_hash_info(key_hash, metadata.prev_hash_range, metadata.hash);


		//check the undetectable region first
		if(compare(key_hash, zeros)>0  && compare(key_hash, metadata.hash)<=0){
			if(metadata == metadata_entries.get(0))
				return true;
		}

		if(compare(key_hash, ring_size)<=0 && compare(key_hash, metadata.prev_hash_range)>0){
			if(metadata == metadata_entries.get(0))
				return true;
		}


		//if key is not within the undetectable range and it's the normal case
		if(compare(key_hash,metadata.prev_hash_range)>0 && compare(key_hash,metadata.hash)<=0){
			logger.info("key is in range");
			return true;
		}else{
			logger.info("key is not in range");
			return false;
		}
	
		
    }
	
	

	/**
	* 
	* find the first successor's 
	* 
	* 
	* @param int our server
	* @param int coord_server
	* @return: True/false - if the server is the replica server
	*/
    public boolean is_first_successor(int our_server, int coord_server){
    
		int size = metadata_entries.size();
		
		if((our_server - coord_server + size)%size == 1){
			return true;
		}else{
			return false;
		}
		
    }
	
	/**
	* 
	* find the second successor's 
	* 
	* @param int our server
	* @param int coord_server
	* @return: True/false - if the server is the replica server
	*/
    public boolean is_second_successor(int our_server, int coord_server){
    
		int size = metadata_entries.size();
		
		if((our_server - coord_server + size)%size == 2){
			return true;
		}else{
			return false;
		}
		
    }
	
	
	/**
	* 
	* find the first successor's metadata
	* 
	* 
	* 
	* @return: metadataEntry - the metadata of the first successor
	*/
    public MetadataEntry find_first_successor(){
    
		//find our server first
	    // Look for our metadata in list of metadata:
		MetadataEntry our_metadata = null;
		int our_server_index = 0;
		for(MetadataEntry cur_metadata : metadata_entries){

			// Check if it is the right entry:
			if( server_hostname.equals(cur_metadata.hostname) &&  port==cur_metadata.port){
				our_metadata = cur_metadata;
				break;
			}
			our_server_index++;

		}
		if(our_metadata == null){
			logger.error("Could not find our metadata in the metadata list.");
			return null;
		}
	
		int size = metadata_entries.size();
		if(size<=1){
			return null;
		}else{
			return metadata_entries.get((our_server_index+1)%size);
		}
    }
	
	
	/**
	* 
	* find the second successor's metadata
	* 
	* 
	* 
	* @return: metadataEntry - the metadata of the second successor
	*/
    public MetadataEntry find_second_successor(){
    
		//find our server first
	    // Look for our metadata in list of metadata:
		MetadataEntry our_metadata = null;
		int our_server_index = 0;
		for(MetadataEntry cur_metadata : metadata_entries){

			// Check if it is the right entry:
			if( server_hostname.equals(cur_metadata.hostname) &&  port==cur_metadata.port){
				our_metadata = cur_metadata;
				break;
			}
			our_server_index++;

		}
		if(our_metadata == null){
			logger.error("Could not find our metadata in the metadata list.");
			return null;
		}
	
		int size = metadata_entries.size();
		
		if(size<=2){
			return null;
		}else{
			return metadata_entries.get((our_server_index+2)%size);
		}
		
    }
	
	
	
	
	/**
     * 
     * print hash info
	 * @param hash of the key
	 * @param hash of the previous node
	 * @param hash of the node 
     * 
     */
	public void print_hash_info(byte[] key_hash, byte[] start_hash, byte[] end_hash){
		
		logger.info("is_key_in_range: key_hash = " + DatatypeConverter.printHexBinary(key_hash));
		logger.info("Start Range   ----     End Range" );
		logger.info(DatatypeConverter.printHexBinary(start_hash) + "------" + DatatypeConverter.printHexBinary(end_hash));
		logger.info("=====");
		logger.info("\n\n");
		
	}
	

    
    /**
     * 
     * Lexicographically compares two byte arrays
     * 
     * @param left_hash: Left hash to compare to right hash
     * @param right_hash
     * 
     * @return: int > 0  : Left comes after right
     * 			int == 0 : They are equal
     * 			int < 0  : Left comes before right
     */
    public int compare(byte[] left_hash, byte[] right_hash) {
		
		/*
		 * Determines which hash comes first
		 */
		for (int i = 0, j = 0; i < left_hash.length && j < right_hash.length; i++, j++) {
			int a = (left_hash[i] & 0xff);
			int b = (right_hash[j] & 0xff);
			if (a != b) {
				return a - b;
			}
		}
		return left_hash.length - right_hash.length;
	}
    
    /**
     * Initialize basic server paramters from command line
     * 
     * @return : Boolean - are we successfully able to create a socket to listen to later
     */
    private boolean initializeServer() {
    	
    	logger.info("Initialize server: " + strategy + " cache size: " + cachesize);
    	try {
            serverSocket = new ServerSocket(port);
            logger.info("Server listening on port: " 
            		+ serverSocket.getLocalPort());    
            return true;
        
        } catch (IOException e) {
        	logger.error("Error! Cannot open server socket:");
            if(e instanceof BindException){
            	logger.error("Port " + port + " is already bound!");
            }
            return false;
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
	
	
	/**
     * Main entry point for the echo server application. 
     * @param args contains the port number at args[0].
     * @param cacheSize specifies how many key-value pairs the server is allowed 
	 *           to keep in-memory (args[1])
	 * @param strategy specifies the cache replacement strategy in case the cache 
	 *           is full and there is a GET- or PUT-request on a key that is 
	 *           currently not contained in the cache. Options are "FIFO", "LRU", 
	 *           and "LFU". (args[2])
     */
    public static void main(String[] args) {
    	
    	try {
			new LogSetup("logs/server.log", Level.ALL);
			
			// Check number of arguments:
			if(args.length != 3) {
				System.out.println("Error! Invalid number of arguments!");
				System.out.println("Usage: Server <port> <cacheSize> <strategy>!");
			} 
			
			else {
			
				
				
				int port = Integer.parseInt(args[0]);
				int cacheSize = Integer.parseInt(args[1]);
				String strategy = args[2];
		
				
				//check cache size
				if(cacheSize<=0){
					System.out.println("Error! Invalid cache size!");
					System.out.println("Cache size has to be greater than 0");
					
				}
				
				// Check that strategy is a valid option
				//
				else if( !(strategy.equals("FIFO") || strategy.equals("LRU") || strategy.equals("LFU")) ){
					
					System.out.println("Error! Invalid strategy option!");
					System.out.println("Options: FIFO LRU LFU");
					
				}
				
				// Invalid strategy:
				else{
					new KVServer(port, cacheSize, strategy).start();
				}
				
				
				
			}
		} 
    	
    	// Catches log setup errors
    	catch (IOException e) {
			System.out.println("Error! Unable to initialize logger!");
			e.printStackTrace();
			System.exit(1);
		} 
    	// Catches invalid port number format
    	catch (NumberFormatException nfe) {
			System.out.println("Error! Invalid argument <port>! Not a number!");
			System.out.println("Usage: Server <port>!");
			System.exit(1);
		}
    }
    
    
    
    
    
    
    
    /**
     * 
     * Sets the level of the root logger to help filter out messages (useful for testing)
     * 
     * @param levelString: The desired logging level
     * 
     * @return: The new level
     */
    public String setLevel(String levelString) {
		
		if(levelString.equals(Level.ALL.toString())) {
			logger.setLevel(Level.ALL);
			return Level.ALL.toString();
		} else if(levelString.equals(Level.DEBUG.toString())) {
			logger.setLevel(Level.DEBUG);
			return Level.DEBUG.toString();
		} else if(levelString.equals(Level.INFO.toString())) {
			logger.setLevel(Level.INFO);
			return Level.INFO.toString();
		} else if(levelString.equals(Level.WARN.toString())) {
			logger.setLevel(Level.WARN);
			return Level.WARN.toString();
		} else if(levelString.equals(Level.ERROR.toString())) {
			logger.setLevel(Level.ERROR);
			return Level.ERROR.toString();
		} else if(levelString.equals(Level.FATAL.toString())) {
			logger.setLevel(Level.FATAL);
			return Level.FATAL.toString();
		} else if(levelString.equals(Level.OFF.toString())) {
			logger.setLevel(Level.OFF);
			return Level.OFF.toString();
		} else {
			return LogSetup.UNKNOWN_LEVEL;
		}
	}
  
}
