package app_kvServer;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import logger.LogSetup;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.PriorityQueue;
import java.util.Map;
import java.util.Comparator;

public class Cache{
	private final int cache_size;
	private String evic_strategy;

	private final Object data_lock = new Object();

	private static HashMap<String,Element> key_value_map;
	private static PriorityQueue<Element> lfu_queue;

	Logger logger = Logger.getRootLogger();


	public Cache(int cacheSize, String strategy) {
		this.cache_size = cacheSize;
		this.evic_strategy = strategy;

		//Anonymous classes are used to override removeEldestEntry so eviction happens 
		//automatically
		if(evic_strategy.equals("FIFO")) {
			this.key_value_map = new LinkedHashMap<String, Element>() {
				@Override
				protected boolean removeEldestEntry(Map.Entry<String,Element> eldest) {
					return size() > cache_size;
				}

			};
		} else if (evic_strategy.equals("LRU")) {
			this.key_value_map = new LinkedHashMap<String, Element>() {
				@Override
				protected boolean removeEldestEntry(Map.Entry<String, Element> eldest) {
					return size() > cache_size;
				}
			};
		} else if (evic_strategy.equals("LFU")) {
			this.key_value_map = new HashMap<String, Element>();
			Comparator<Element> comparator = new ElementComparator();
			this.lfu_queue = new PriorityQueue<Element>(10, comparator);
		}

	}

	public String get(String key) {
		synchronized(data_lock){
			
			Element temp_entry = key_value_map.get(key);

			if(temp_entry != null) {

				//LFU requires removing Element from priorityQueue, 
				//	increment by 1 and adding to both PriorityQueue and Hashmap
				if(evic_strategy.equals("LFU")) {
					lfu_queue.remove(temp_entry);
					temp_entry.incFreq();
					lfu_queue.add(temp_entry);
					key_value_map.put(key, temp_entry);
				}
			
				return temp_entry.getVal();
			}
			return null;
		}
	}

	public String put(String key, String value) {
		synchronized(data_lock) {
			//Check if the cache if the key value exists
			Element temp_entry = key_value_map.get(key);
			String return_val;
			if(temp_entry != null)
			{
				return_val = temp_entry.getVal();
				temp_entry.setVal(value);
				temp_entry.incFreq();
			} else {
				temp_entry = new Element(key,value,1);
				key_value_map.put(key, temp_entry);
				return_val = temp_entry.getVal();
			}
			
			
			//Check if cache type is LFU. LRU and FIFO does removal automatically
			if(evic_strategy.equals("LFU")) {
				int curr_size = key_value_map.size();
	
				if(curr_size >= cache_size) {
					//Remove an entry first
					Element removed_entry = lfu_queue.poll();
					if(removed_entry != null) {
						key_value_map.remove(removed_entry.getKey());
					} 
				}


			}

			return return_val;
		}
	}

	public String remove(String key) {
		synchronized(data_lock) {
			Element temp_entry = key_value_map.get(key);
			if(temp_entry != null)
				return key_value_map.remove(key).getVal();

			return null; 
		}
	}
}

//Implementation for the priority queue counting frequency 

class Element 
{
	private String value;
	private String key;
	private int freq;

	public Element(String key, String val, int frequency)
	{
		this.key = key;
		this.value = val;
		this.freq = frequency;
	}	

	public String getVal(){
		return value;
	}

	public int getFreq() {
		return freq;
	}

	public String getKey() {
		return key;
	}

	public void setVal(String val)
	{
		value = val;
	}	

	public void incFreq()
	{
		freq++;
	}

	public void decFreq()
	{
		freq--;
	}
}

class ElementComparator implements Comparator<Element>
{
	@Override
	public int compare(Element x, Element y)
	{
		return x.getFreq() - y.getFreq();
	}
}
