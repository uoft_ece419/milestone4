package testing;

import org.junit.Test;

import app_kvEcs.ECSClient;
import app_kvEcs.ECSFunc;
import client.KVStore;

import junit.framework.TestCase;
import java.io.IOException;
import common.messages.KVMessage;
import common.messages.KVMessage.StatusType;

//Fixed Method Order is used to ensure continuity of deletes and puts

public class AdditionalTest extends TestCase {
	
	/***
	 *	@config file testECS.config sets up 4 servers 
	 *	
	 *
	 */
	
	//Use the objects for EF and EC to test 
	private ECSClient EClient = AllTests.EC;
	private ECSFunc EFunc = AllTests.EF;

	private KVStore kvc1;
	private KVStore kvc2;
	private KVStore kvc3;
	private KVStore kvc4;
	

	//Make sure the servers have been initialized and started
	public void setUp() {

		try {
			EClient.handle_command("start", EFunc);

			//Test multiple connections
			kvc1 = new KVStore("localhost", 40501);
			kvc2 = new KVStore("localhost", 40502);
			kvc3 = new KVStore("localhost", 40503);
			kvc4 = new KVStore("localhost", 40504);
		

			kvc1.connect();
			kvc2.connect();
			kvc3.connect();
			kvc4.connect();

		} catch (Exception e) {
			
		}
	}

	//replication test 0
	//very basic simple test: put with key value and then get from the itself
	public void test_replication_simple0() {

		Exception ex = null;

		String key = "hello";
		String value = "world";
		KVMessage response0 = null;
		KVMessage response1 = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		KVMessage response4 = null;
		

		try {
			kvc2.connect();

			//use coordinator for put
			response0 = kvc2.put(key, value);
			
		} catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		
		
		//test with redirections
		//because of redirection, all servers should pass
		try{
			response1 = kvc2.get("hello");
		}catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		
	}
	
	
	
	//replication test 1
	//very basic simple test: put with key value and then get from the replica
	public void test_replication_simple() {

		Exception ex = null;

		String key = "hello";
		String value = "world";
		KVMessage response0 = null;
		KVMessage response1 = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		KVMessage response4 = null;
		

		try {
			kvc2.connect();

			//use coordinator for put
			response0 = kvc2.put(key, value);
			
		} catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		
		
		//test with redirections
		//because of redirection, all servers should pass
		try{
			response1 = kvc1.get("hello");
		}catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		
	}
	
	
	
	
	
	
	//replication test 2
	//coordinator server port number for "hello" is 40502, kvserver2
	//the first and second successors are kvserver1 and kvserver 3
	public void test_replication_basic() {

		Exception ex = null;

		String key = "hello";
		String value = "world";
		KVMessage response0 = null;
		KVMessage response1 = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		KVMessage response4 = null;
		

		try {
			kvc1.connect();
			kvc2.connect();
			kvc3.connect();
			kvc4.connect();

			//use coordinator for put
			response0 = kvc2.put(key, value);
			
			//getting the key without redirection
			response1 = kvc1.get_func("hello");
			response2 = kvc2.get_func("hello");
			response3 = kvc3.get_func("hello");
			response4 = kvc4.get_func("hello");
			
		} catch (Exception e) {
			ex = e; 
		}
		
		// System.out.println(response1.getStatus());
		// System.out.println(response2.getStatus());
		// System.out.println(response3.getStatus());
		// System.out.println(response4.getStatus());
		
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
		
		
		//test with redirections
		//because of redirection, all servers should pass
		try{
			response1 = kvc1.get("hello");
			response2 = kvc2.get("hello");
			response3 = kvc3.get("hello");
			response4 = kvc4.get("hello");
		}catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.GET_SUCCESS && response4.getValue().equals(value));
		
	}
	
	
	//replication test 3
	//update a value at a server. The replicas and coordinator should be able to provide
	//new value and non-replica shouldn't.
	public void test_replication_update() {

		Exception ex = null;

		String key = "hello";
		String value = "world";
		KVMessage response0 = null;
		KVMessage response1 = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		KVMessage response4 = null;
		

		try {
			kvc1.connect();
			kvc2.connect();
			kvc3.connect();
			kvc4.connect();

			//use coordinator for put
			response0 = kvc2.put(key, value);
			
			//getting the key without redirection
			response1 = kvc1.get_func("hello");
			response2 = kvc2.get_func("hello");
			response3 = kvc3.get_func("hello");
			response4 = kvc4.get_func("hello");
			
		} catch (Exception e) {
			ex = e; 
		}
		
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
		
		//test with redirections
		//because of redirection, all servers should pass
		try{
			response1 = kvc1.get("hello");
			response2 = kvc2.get("hello");
			response3 = kvc3.get("hello");
			response4 = kvc4.get("hello");
		}catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.GET_SUCCESS && response4.getValue().equals(value));

	}
	
	
	//replication test 4
	public void test_replication_deletion() {

		Exception ex = null;

		String key = "hello";
		String value = "null";
		KVMessage response0 = null;
		KVMessage response1 = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		KVMessage response4 = null;
		

		try {
			kvc1.connect();
			kvc2.connect();
			kvc3.connect();
			kvc4.connect();

			//use coordinator for put
			response0 = kvc2.put(key, value);
			
			//getting the key without redirection
			response1 = kvc1.get_func("hello");
			response2 = kvc2.get_func("hello");
			response3 = kvc3.get_func("hello");
			response4 = kvc4.get_func("hello");
			
		} catch (Exception e) {
			ex = e; 
		}
		
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_ERROR && response1.getValue() == null);
		assertTrue(response2.getStatus() == StatusType.GET_ERROR && response2.getValue() == null);
		assertTrue(response3.getStatus() == StatusType.GET_ERROR && response3.getValue() == null);
		assertTrue(response4.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
		
		//test with redirections
		//because of redirection, all servers should pass
		try{
			response1 = kvc1.get("hello");
			response2 = kvc2.get("hello");
			response3 = kvc3.get("hello");
			response4 = kvc4.get("hello");
		}catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_ERROR && response1.getValue() == null);
		assertTrue(response2.getStatus() == StatusType.GET_ERROR && response2.getValue() == null);
		assertTrue(response3.getStatus() == StatusType.GET_ERROR && response3.getValue() == null);
		assertTrue(response4.getStatus() == StatusType.GET_ERROR && response4.getValue() == null);

	}
	
	
	//replication test 5
	//Client does get (key, value) at replica or coordinator. He shouldnt be redirected to coordinator.
	public void test_replication_get_redirection() {

		Exception ex = null;

		String key = "hello";
		String value = "world";
		KVMessage response0 = null;
		KVMessage response1 = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		KVMessage response4 = null;
		

		try {
			kvc1.connect();
			kvc2.connect();
			kvc3.connect();
			kvc4.connect();

			//use coordinator for put
			response0 = kvc2.put(key, value);
			
			//getting the key without redirection
			response1 = kvc1.get_func("hello");
			response2 = kvc2.get_func("hello");
			response3 = kvc3.get_func("hello");
			response4 = kvc4.get_func("hello");
			
		} catch (Exception e) {
			ex = e; 
		}
		
		
		//getting kv with success for the first time means that there is no re-direction
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);

	}
	
	//replication test 6
	//Client does put (key, value) at non-coordinator or replica. He should be redirected to coordinator.
	public void test_replication_put_redirection() {

		Exception ex = null;

		String key = "hello";
		String value = "world";
		KVMessage response0 = null;
		KVMessage response1 = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		KVMessage response4 = null;
		

		try {
			kvc1.connect();
			kvc2.connect();
			kvc3.connect();
			kvc4.connect();

			//use coordinator for put, no redirection
			response2 = kvc2.put_func(key, value);
			
			//use replicas for put, expect redirection
			response1 = kvc1.put_func(key, value);
			response3 = kvc3.put_func(key, value);
			
			//use non-replica/non-coordinator for put, expect redirection
			response4 = kvc4.put_func(key, value);
			
		} catch (Exception e) {
			ex = e; 
		}
	
		
		//getting kv with success for the first time means that there is no re-direction
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
		assertTrue(response2.getStatus() == StatusType.PUT_SUCCESS);
		assertTrue(response3.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
		assertTrue(response4.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
		
		
		//test with redirections
		//because of redirection, all servers should pass
		try{
			response1 = kvc1.put(key, value);
			response2 = kvc2.put(key, value);
			response3 = kvc3.put(key, value);
			response4 = kvc4.put(key, value);
		}catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.PUT_SUCCESS);
		assertTrue(response2.getStatus() == StatusType.PUT_SUCCESS);
		assertTrue(response3.getStatus() == StatusType.PUT_SUCCESS);
		assertTrue(response4.getStatus() == StatusType.PUT_SUCCESS);

	}
	
	
	//replication test 7
	//adding a node and then do the same replication testing
	public void test_replication_addNode(){
		
		Exception ex = null;
		
		//connect to the new server
		KVStore kvc5 = new KVStore("localhost", 40505);
		try {
			EClient.handle_command("addNode 1024 FIFO", EFunc);
			EClient.handle_command("start", EFunc);
			kvc5.connect();
		} catch (Exception e) {
			ex = e;
		}
		assertNull(ex);
		
		//then do the replication test
		String key = "hello";
		String value = "world";
		KVMessage response0 = null;
		KVMessage response1 = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		KVMessage response4 = null;
		KVMessage response5 = null;
		

		try {
			kvc1.connect();
			kvc2.connect();
			kvc3.connect();
			kvc4.connect();

			//use coordinator for put
			response0 = kvc2.put(key, value);
			
			//getting the key without redirection
			response1 = kvc1.get_func(key);
			response2 = kvc2.get_func(key);
			response3 = kvc3.get_func(key);
			response4 = kvc4.get_func(key);
			response5 = kvc5.get_func(key);
			
		} catch (Exception e) {
			ex = e; 
		}
		
		// System.out.println(response1.getStatus());
		// System.out.println(response2.getStatus());
		// System.out.println(response3.getStatus());
		// System.out.println(response4.getStatus());
		// System.out.println(response5.getStatus());
		
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
		assertTrue(response5.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
		
		//test with redirections
		//because of redirection, all servers should pass
		try{
			response1 = kvc1.get(key);
			response2 = kvc2.get(key);
			response3 = kvc3.get(key);
			response4 = kvc4.get(key);
			response5 = kvc5.get(key);
		}catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.GET_SUCCESS && response4.getValue().equals(value));
		assertTrue(response5.getStatus() == StatusType.GET_SUCCESS && response5.getValue().equals(value));
		
		
	}
	
	
	
	//replication test 8
	//remove a node and then do the same replication testing
	public void test_replication_removeNode(){
		
		Exception ex = null;
		
		//first remove a node
		try {
			EClient.handle_command("removeNode", EFunc);
		} catch (Exception e) {
			ex = e;
		}
		assertNull(ex);
		
		//then do the replication test
		String key = "hello";
		String value = "world";
		KVMessage response0 = null;
		KVMessage response1 = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		KVMessage response4 = null;
		

		try {
			kvc1.connect();
			kvc2.connect();
			kvc3.connect();
			kvc4.connect();

			//use coordinator for put
			response0 = kvc2.put(key, value);
			
			//getting the key without redirection
			response1 = kvc1.get_func(key);
			response2 = kvc2.get_func(key);
			response3 = kvc3.get_func(key);
			response4 = kvc4.get_func(key);
			
		} catch (Exception e) {
			ex = e; 
		}
		
		// System.out.println(response1.getStatus());
		// System.out.println(response2.getStatus());
		// System.out.println(response3.getStatus());
		// System.out.println(response4.getStatus());
		
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
		
		//test with redirections
		//because of redirection, all servers should pass
		try{
			response1 = kvc1.get(key);
			response2 = kvc2.get(key);
			response3 = kvc3.get(key);
			response4 = kvc4.get(key);
		}catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.GET_SUCCESS && response4.getValue().equals(value));
		
		
	}
	
	
	//replication test 9
	//more complex replication testing - adding and then remove a node to test consistency
	public void test_replication_add_remove_node(){
		
		Exception ex = null;
		
		//first remove a node
		try {
			EClient.handle_command("addNode 1024 FIFO", EFunc);
			EClient.handle_command("removeNode", EFunc);
		} catch (Exception e) {
			ex = e;
		}
		assertNull(ex);
		
		//then do the replication test
		String key = "hello";
		String value = "world";
		KVMessage response0 = null;
		KVMessage response1 = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		KVMessage response4 = null;
		

		try {
			kvc1.connect();
			kvc2.connect();
			kvc3.connect();
			kvc4.connect();

			//use coordinator for put
			response0 = kvc2.put(key, value);
			
			//getting the key without redirection
			response1 = kvc1.get_func(key);
			response2 = kvc2.get_func(key);
			response3 = kvc3.get_func(key);
			response4 = kvc4.get_func(key);
			
		} catch (Exception e) {
			ex = e; 
		}
		
		// System.out.println(response1.getStatus());
		// System.out.println(response2.getStatus());
		// System.out.println(response3.getStatus());
		// System.out.println(response4.getStatus());
		
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
		
		//test with redirections
		//because of redirection, all servers should pass
		try{
			response1 = kvc1.get(key);
			response2 = kvc2.get(key);
			response3 = kvc3.get(key);
			response4 = kvc4.get(key);
		}catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex == null);
		assertTrue(response1.getStatus() == StatusType.GET_SUCCESS && response1.getValue().equals(value));
		assertTrue(response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
		assertTrue(response3.getStatus() == StatusType.GET_SUCCESS && response3.getValue().equals(value));
		assertTrue(response4.getStatus() == StatusType.GET_SUCCESS && response4.getValue().equals(value));
		
		
	}
	

	//failure testing 1
	//kill one server and try the get operation
	// public void test_failure_shutdown(){
		
		// Exception ex = null;
		
		// try {
			// EClient.handle_command("shutdown", EFunc);
		// } catch (Exception e) {
			// ex = e;
		// }
		// assertNull(ex);
		
		// //then do the replication test
		// String key = "hello";
		// String value = "world";
		
		
		// try {
			// EClient.handle_command("start", EFunc);
		// } catch (Exception e) {
			// ex = e;
			// e.printStackTrace();
		// }
		// assertNull(ex);
	
		
		
	// }

	
	
	
	
}
