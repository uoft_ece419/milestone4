package testing;

import java.io.IOException;

import org.apache.log4j.Level;

import app_kvServer.KVServer;
import logger.LogSetup;

import app_kvEcs.ECSClient;
import app_kvEcs.ECSFunc;

public class ServerPerf{

	public static void main(String[] args)
	{
		String config_file = args[0];
		int num_servers = Integer.parseInt(args[1]);
		int cache_size = Integer.parseInt(args[2]);
		int repeat_times = Integer.parseInt(args[3]);
		String cache_type = args[4];

		String master_host = args[5];
		int master_port = Integer.parseInt(args[6]);

		try {
			new LogSetup("logs/testing/test.log", Level.ERROR);
		} catch (Exception e) {

		}
	
		//Set up of the initial Conditions
		ECSClient EC = new ECSClient();
		ECSFunc EF = new ECSFunc(true, master_host, master_port, "localhost", 55555);
	
		ECSClient.parse_configuartion_file(EC, config_file);
		ECSClient.parse_master_file(EC, "./master_server.config");
		//`EC.failure_server=null;
	
	 	String init_string = "initService 1 " + cache_size + " " + cache_type;

		EC.handle_command(init_string, EF);
		//EC.handle_command("start", EF);

		long start_time;
		long end_time;
		
		long avg_add;
		long avg_remove;

		//Loop through num_servers adding and remove nodes each time. 
		//repeat_times of addNode/removeNode is used to obtain an average
 		for(int i=0; i < num_servers - 1; i++)
		{
			avg_add = 0;
			avg_remove = 0;
		
			for(int j = 0; j < repeat_times; j++) 
			{
				start_time = System.nanoTime(); 

				String addString = "addNode " + cache_size + " " + cache_type;
				
				EC.handle_command(addString, EF);

				end_time = System.nanoTime();

				avg_add += end_time - start_time;

			
				start_time = System.nanoTime();
				
				EC.handle_command("removeNode", EF);
			
				end_time = System.nanoTime();

				avg_remove += end_time - start_time;

			}

			avg_add = avg_add/repeat_times;
			avg_remove = avg_remove/repeat_times;
			
	
			System.out.println("Server #: " + (i+1) + " add time: " + avg_add + " remove time: " + avg_remove);
		}

		EC.handle_command("shutdown", EF);
		EC.handle_command("quit", EF);

	}
}
