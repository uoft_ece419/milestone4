package testing;

import java.io.*;
import java.io.IOException;
import client.KVStore;
import app_kvClient.KVClient;
import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import logger.LogSetup;
import common.messages.KVMessage;

class GetWorker implements Runnable {
	private Thread t;
	private String threadName;
	private int portNum;
	private int startingLineNum;
	private String hostName;
	private String [] keys;
	private String [] values;
	private KVClient KVC;

	//Worker Creation
	GetWorker(int threadNum, int port, String host) {
		//System.out.println("Creating threads");

		threadName = Integer.toString(threadNum);
		portNum = port;
		hostName = host;
	}

	public void setKeys(String [] k)
	{
		keys = k;
	}	
	
	public void setValues(String [] v)
	{
		values = v;
	}
	
	public void run() {

		long avg_time = 0;

		try {
			KVC = new KVClient();

			String connect_string = "connect " + hostName + " " + portNum;

			System.out.println(connect_string);

			KVC.handle_command(connect_string);

			//Run the following operations
			long avg_put = exe_operation("PUT");

			long avg_get = exe_operation("GET");

			long avg_delete = exe_operation("DELETE");

			//Output results
			System.out.println("Thread " + threadName + ": Get:" + avg_get + " PUT:" + avg_put + " DELETE:" + avg_delete);

			KVC.handle_command("disconnect");
			KVC.handle_command("quit");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void start() {
		if(t == null) {
			t = new Thread (this, threadName);
			t.start();
		}
	}

	public long exe_operation(String type) {
		
		int num_KV_pairs = keys.length;

		long startTime = System.nanoTime();

		KVMessage message = null;

		//Loop through the key-value pairs and perform GET/PUT/DELETE
		for(int i =0; i < num_KV_pairs; i++) {

			try {
		
				if(type.equals("GET"))
					KVC.handle_command("get " + keys[i]);
				else if (type.equals("PUT"))
					KVC.handle_command("put " + keys[i] + " " + values[i]);
				else if(type.equals("DELETE"))
					KVC.handle_command("put " + keys[i]);
			} catch (Exception e) {
				System.out.println("Error: KEY: " + keys[i]);
			}	

		}

		long endTime = System.nanoTime();

		long avg_time = endTime - startTime;

		avg_time = avg_time / keys.length;

		return avg_time;
	}
}
public class perf {

	public static void main(String[] args) {

	//Parameters for the client perf test
	int numWorkers = Integer.parseInt(args[0]);
	int opNum = Integer.parseInt(args[1]);
	int startLine = Integer.parseInt(args[2]);
	String pathString = args[3];
	String hostAddr = args[4];
	int portNum = Integer.parseInt(args[5]);

	String[][] keys = new String[numWorkers][opNum];
	String[][] values = new String[numWorkers][opNum];

	String line;

	try {

		new LogSetup("logs/perf.log", Level.ERROR);
	

		BufferedReader sec_reader = new BufferedReader(new FileReader(pathString));

		//Finding the correct starting place
		for(int i =0; i < startLine; i++)
		{
			sec_reader.readLine();
		}

		//Read the KV-values out of enron emails file. Split them up to the worker objects
		for(int i = 0; i < numWorkers; i++)
		{
			for(int j = 0; j < opNum; j++)
			{
				if((line = sec_reader.readLine()) != null);
				{
					String key[] = line.split(",");
					keys[i][j] = key[0].substring(1, 20);
					values[i][j] = key[1];
					//System.out.println("key: "+ key[0] + " value: (" + key[1] + ")");
				}
			}
		}
	
		System.out.println("Finished loading worker keys");

		System.out.println("Starting workers");

		//Run the workers
		for(int counter = 0; counter < numWorkers; counter++) {
			GetWorker worker = new GetWorker(counter, portNum, hostAddr);
			worker.setKeys(keys[counter]);
			worker.setValues(values[counter]);
			worker.start();
		}
		
	} catch (Exception e) {

	}	
	}
}


