package app_kvEcs;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import app_kvEcs.ECSFunc.ServerConnection;
import app_kvServer.ClientConnection;
import common.messages.ECSMessage;
import common.messages.MetadataEntry;
import common.messages.ECSMessage.ECS_Status;
import common.messages.TextMessage;
import common.messages.TextMessage.MessageParameters;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

/*
 * Upon failure of a KVServer, this thread class will be notified and:
 * 
 * 1) Block the ECSClient from performing any actions, 
 * 2) Correct the state of replication
 * 3) Add a new KVServer from the pool of available servers if possible.
 * 
 * 
 */
public class FailureServer extends Thread {
	
	// Allows us to use replication functionality when handling failed servers:
	//
	FailureHandler failure_api = new FailureHandler();
	List<ServerEntry> available_servers = null; // Points to pool of available servers from ECSClient
	 
	
	
	// What the failure server listens on for connections:
	String hostname;
	int port;

	// Logging events:
	static Logger logger = Logger.getRootLogger();

	// Server socket to accept connections:
	private ServerSocket serverSocket;
	private Socket clientSocket;
	private boolean isOpen = true;

	
	// Receiver buffer/message parameters:
	private static final int BUFFER_SIZE = 1024;
	private static final int DROP_SIZE = 1024 * BUFFER_SIZE;
	public static final byte HEADER_OPEN = 0x7E;
	public static final byte HEADER_CLOSE = 0x5E;
	public static final byte MESSAGE_STUFFING = 0x23;
	private static final String PROMPT = "ECS-Client> ";
	private OutputStream output;
	private InputStream input;
	
	
	
	// Used to update the current open server connections:
	ECSFunc ecs_api = null;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * FailureServer Constructor
	 * 
	 * @param _hostname:
	 *            What the failure server listens on for a hostname
	 * @param _port:
	 *            ... for a port.
	 */
	public FailureServer(String _hostname, int _port, ECSFunc _ecs_api, List<ServerEntry> _available_servers) {
		this.hostname = _hostname;
		this.port = _port;
		this.ecs_api = _ecs_api;
		this.available_servers = _available_servers;
	}

	
	
	
	
	/*
	 * Main loop of execution:
	 */
	public void run() {

		// IF we successfully open the server socket:
		//
		if (initializeServer()) {

			while (isOpen) {
				try {

					// Its possible we're running but the socket is null:
					if (serverSocket != null) {

						
						
						/*
						 * KVServer connects:
						 */
						clientSocket = null;
						try {
							clientSocket = serverSocket.accept();
						} catch (SocketException e) {
							logger.info("Closed the server in accept call. \n", e);
						}
					

						
						
						
						
						
						// On successful server connect:
						// (A KVServer is likely connecting in order to alert us)
						//
						if (serverSocket != null && clientSocket != null) {
							

							// Send a message acknowledging the successful connection:
							//
							logger.info("\n\nFailure server received a connection.\n\n");
							input = clientSocket.getInputStream();
							output = clientSocket.getOutputStream();
							sendMessage(new TextMessage(
									"Connection to MSRG Echo server established: " 
									+ clientSocket.getLocalAddress() + " / "
									+ clientSocket.getLocalPort(), null, null, null, false, false));
							logger.info("\n\nSent a \"connection to failure server\" message.\n\n");
							
							
							
							
							
							// Received a failure alert possibly:
							TextMessage alert = receiveMessage();
							
							
							// If the failure detector hasn't died ---> sent us a message:
							if(alert != null){
								
								// IF it is an ECS type message:
								if(alert.getisECS()){

									
									// Unpack the message:
									String encapsulated_ECS_message = alert.getMsg();
									Gson gson = new Gson();
									ECSMessage json_parameters = gson.fromJson(encapsulated_ECS_message, ECSMessage.class);						
									ECS_Status request = json_parameters.get_message_type();
									
									
									
									// If a failure warning:
									if(request == ECS_Status.FAILURE_WARNING){
										
										logger.info("\n\nFailure server received a failure warning.\n\n");
										// Send an ACK and check for failures:
										ECSMessage encapsulated_message = new ECSMessage(ECS_Status.FAILURE_WARNING_ACK, null, null, null, null, null, -1, null, -1);
										
										// Create encapsulated string:
										gson = new Gson();
										String encapsulated_string = gson.toJson(encapsulated_message);
										
										// Send TextMessage:
										TextMessage message_to_send = new TextMessage(encapsulated_string, null,null,null,true, false); 
										sendMessage(message_to_send);
										

										/*
										 * Avoid race conditions with ECSClient.
										 */
										try{
											ecs_api.acquire_ECS_control();
											failure_api.handle_failure_warning(ecs_api, available_servers); // Check for any server failures
																											// and handle them appropriately.		
										}
										finally{
											ecs_api.release_ECS_control();
										}
										
										clientSocket.close();
										System.out.print("\n\n");
										System.out.print(PROMPT);
											
									}
									else{
										logger.info("Failure server received an indiscernable message of type " + request + ".");
									}
										
								}
							}
						}
						
						
						
						
					}

				} catch (IOException e) {
					logger.error("Error! " + "Unable to establish connection. \n", e);
				}
				catch(Exception e){
					logger.fatal(e);
				}

			}
		} else {
			logger.error("Failed to start failure server (ironically)...");
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Initialize basic server paramters from command line
	 * 
	 * @return : Boolean - are we successfully able to create a socket to listen
	 *         to later
	 */
	private boolean initializeServer() {

		System.out.println("Initializing failure server...");
		InetAddress address = null;
		try {
			address = InetAddress.getByName(hostname);
		} catch (UnknownHostException e) {
			logger.error(e);
			logger.error("Could not use hostname: " + hostname + ".");
			return false;
		}

		try {
			serverSocket = new ServerSocket(port, 50, address);
			System.out.println("Failure server listening on port: " + serverSocket.getLocalPort());
			return true;

		} catch (IOException e) {
			logger.error("Error! Cannot open server socket:");
			if (e instanceof BindException) {
				logger.error("Port " + port + " is already bound!");
			}
			return false;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Method sends a TextMessage using this socket.
	 * @param msg the message that is to be sent.
	 * @throws IOException some I/O error regarding the output stream 
	 */
	public void sendMessage(TextMessage msg) throws IOException {
		byte[] msgBytes = msg.getMsgBytes();
		output.write(msgBytes, 0, msgBytes.length);
		output.flush();
		String string_format = new String(msgBytes);
		logger.trace("SEND <" 
				+ clientSocket.getInetAddress().getHostAddress() + ":" 
				+ clientSocket.getPort() + ">:" 
				+ string_format);
    }
	
	
	
	
	
	
	
	

	/**
	 * Attempts to wait and read a request from a client.
	 * 
	 * @return: TextMessage - formatted message received 
	 * 			NULL - indicating a closed connection
	 * 
	 * @throws IOException: If there is an external failure reading from the socket
	 */
	private TextMessage receiveMessage() throws IOException {
		
		// Message to return is initially null to indicate failure reading bytes:
		TextMessage msg = null;
		
		
		int index = 0;
		byte[] msgBytes = null, tmp = null;
		byte[] bufferBytes = new byte[BUFFER_SIZE];
		byte cur_byte = 0x0;
		byte prev = 0x0;
		
		/* read first char from stream */
		boolean reading = true;


		// While we haven't read the opening header for a message, keep reading before we record anything
		//
		while(!(cur_byte == HEADER_OPEN && prev != MESSAGE_STUFFING) && reading){
			prev = cur_byte;
			cur_byte = (byte)input.read();
			

			if(cur_byte == -1){
				reading = false;
				isOpen = false;
			}
		}
		
		// Error while reading, ie client close:
		if(!isOpen)
			return msg;
		
		
		
		/* read next char from stream */
		cur_byte = (byte) input.read();
		
				
		
		
		// While we haven't read the closing header for the message, record everything, this is the JSON object
		//
		boolean stuff_last_char = false;
		
		while(!(cur_byte == HEADER_CLOSE &&  prev!= MESSAGE_STUFFING) && reading) {
			
			
			// IF we enter and we previously skipped writing because of a stuff
			// The stuff is an actual character within the message:
			//
			if(stuff_last_char){

				// This was not an attempt to stuff a header open either:
				//
				if(cur_byte != HEADER_OPEN){
					bufferBytes[index] = cur_byte;
					prev = cur_byte;
							
					index++;
				}
			}
			
			
			
			// Update whether the current character is a stuff character
			if(cur_byte == MESSAGE_STUFFING)
				stuff_last_char = true;
			else
				stuff_last_char = false;

				
			
			
			if(!stuff_last_char){
					
				/* CR, LF, error */
				/* if buffer filled, copy to msg array */
				if(index == BUFFER_SIZE) {
					if(msgBytes == null){
						tmp = new byte[BUFFER_SIZE];
						System.arraycopy(bufferBytes, 0, tmp, 0, BUFFER_SIZE);
					} else {
						tmp = new byte[msgBytes.length + BUFFER_SIZE];
						System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
						System.arraycopy(bufferBytes, 0, tmp, msgBytes.length,
								BUFFER_SIZE);
					}
	
					msgBytes = tmp;
					bufferBytes = new byte[BUFFER_SIZE];
					index = 0;
				} 
				
				/* only read valid characters, i.e. letters and constants */
				bufferBytes[index] = cur_byte;						
				index++;
				
				/* stop reading is DROP_SIZE is reached */
				if(msgBytes != null && msgBytes.length + index >= DROP_SIZE) {
					reading = false;
				}
			
			}
			
			prev = cur_byte;
			/* read next char from stream */
			cur_byte = (byte) input.read();
			
			if(cur_byte == -1){
				reading = false;
				isOpen = false;
			}
				
		}
		
		// Error reading bytes, ie client close:
		if(!isOpen)
			return msg;
		
		
		
		if(msgBytes == null){
			tmp = new byte[index];
			System.arraycopy(bufferBytes, 0, tmp, 0, index);
		} else {
			tmp = new byte[msgBytes.length + index];
			System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
			System.arraycopy(bufferBytes, 0, tmp, msgBytes.length, index);
		}
		
		
		// MsgBytes contains actual message which is a JSON string
		//
		// We convert the JSON string into a MessageParameters object for convenience
		//
		msgBytes = tmp;
		String string_format = new String(msgBytes);
		
		Gson gson = new Gson();
		MessageParameters json_parameters = gson.fromJson(string_format, MessageParameters.class);
		
		
		/* build final String */
		msg = new TextMessage(json_parameters.get_msg(), json_parameters.get_key(), json_parameters.get_value(), json_parameters.get_status(), json_parameters.get_isECS(), json_parameters.get_isServer());
		
		logger.info("RECEIVE \t<" 
				+ clientSocket.getInetAddress().getHostAddress() + ":" 
				+ clientSocket.getPort() + ">: '" 
				+ string_format + "'");
				
		return msg;		

    }
	
}
