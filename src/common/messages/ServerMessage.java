package common.messages;

import common.messages.ECSMessage.ECS_Status;

/*
 * Used specifically for server to server communication ---> encapsulated in a TextMessage.
 */
public class ServerMessage {

	/*
	 * 
	 * Possible messages types sent between KVServer and KVServer
	 */
	public enum KVS_Status{
		
		HEARTBEAT,				// A test of whether a server is currently alive and the corresponding ACK.
		HEARTBEAT_ACK,
		
		
		DONE_TRACKING,			// Used to indicate that this server is no longer being tracked by the current Socket connection.
		DONE_TRACKING_ACK
	}
	KVS_Status message_type;
	
	
	/*
	 * ServerMessage constructor
	 */
	public ServerMessage(KVS_Status type){
		this.message_type = type;
	}
	
	
	
	
	
	
	/*
	 * Accessor functions:
	 */
	public KVS_Status get_message_type(){
		return this.message_type;
	}
}
